{pkgs, ...}:
{
  require = [
    ./audioAlsa.nix
    ./defaultEditor.nix
    ./fontConfig.nix
    ./services.nix
    ./standardEnv.nix
    ./userOpt.nix
    ./pfKernel.nix
  ];
  nix.extraOptions = ''
    auto-optimise-store = true
    env-keep-derivations = false
    gc-keep-outputs = false
    gc-keep-derivations = false
  ''; 
  users.defaultUserShell = "/run/current-system/sw/bin/zsh";
  programs.bash = {
    promptInit = "PS1=\"# \"";
    enableCompletion = true;
  };
  system.autoUpgrade.enable = true;
}

