{ config, pkgs, fetchurl, ... }:

  let  linuxPackages_customWithPatches = {version, src, configfile, kernelPatches}:
    let linuxPackages_self = 
      (pkgs.linuxPackagesFor (pkgs.linuxManualConfig {inherit version src configfile kernelPatches;
      allowImportFromDerivation=true;})
    linuxPackages_self);
  in pkgs.recurseIntoAttrs linuxPackages_self;
in {
  boot.kernelPackages = linuxPackages_customWithPatches {
    version = "4.2.3-zen";
    src = pkgs.fetchurl {
      url = "https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.2.tar.xz";
      sha256 = "1syv8n5hwzdbx69rsj4vayyzskfq1w5laalg5jjd523my52f086g";
    };
    configfile = /etc/nixos/configs/kernels/kernel.config;
    kernelPatches = [
      { patch = /etc/nixos/configs/kernels/patches/4.2.3-3.patch; name = "liquorix-patch"; }
    ];
  };

}
