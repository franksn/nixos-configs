{ config, pkgs, lib, ... } :

with lib;

{
  options =
  {
    environment.editorIsVim = mkOption
    {
      default = true;
      example = true;
      type = with types; bool;
      description = ''
          Vim for default, Emacs for everything else .
      '';
    };
  };

  config = mkIf config.environment.editorIsVim
  {
    environment.shellInit = ''
        export EDITOR=vim
    '';
    environment.systemPackages = [ pkgs.vim ];
  };
}


