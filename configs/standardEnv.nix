{config, pkgs, lib, ...}:
with lib;
{
  config =
  {
    environment.shellInit = ''
      export LC_ALL=${config.i18n.defaultLocale}
    '';
    security.sudo.enable = true;
    security.sudo.wheelNeedsPassword = false;

    nixpkgs.config.allowUnfree = true;
  };
}
