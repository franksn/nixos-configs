{configs,pkgs,...}:
{
  ## Xorg service configs
  services.xserver = {
    autorun = true;
    enable = true;
    layout = "us";
    videoDrivers = [ "intel" ];
    vaapiDrivers = [ pkgs.vaapiIntel ];
    exportConfiguration = true;
    desktopManager = {
      xterm.enable = false;
      default = "none";
    };
    displayManager.slim = {
        defaultUser = "franksn";
      };
    # Things to do before starting the session
    displayManager.sessionCommands = ''
      xrdb -load /home/franksn/.config/X11/Xresources &
      xset fp default
      [[ -d ~/.local/share/fonts ]] && xset +fp ~/.local/share/fonts
      xset fp rehash
      urxvtd -q -o -f &
      hsetroot -solid "#b2a691" &
      compton -bcGf -r 5 -l -2 -t -2 -o 0.88 --no-fading-openclose --no-dnd-shadow --no-dock-shadow &
      ##sxhkd -c /home/franksn/.config/sxhkd/sxhkdrc &
    '';
    xkbOptions = "terminate:ctrl_alt_bksp";

    # i don't know how to enable wmutils in nixos, guess i'll stick to bspwm.
    windowManager = {
      bspwm.enable = true;
      default = "bspwm";
    };

    startGnuPGAgent = true;
   };

  # Enable OpenSSH
  # services.openssh.enable = true;

  # Program ssh agent
  programs.ssh.startAgent = false;

  # Httpd section
  services.httpd = {
    enable = true;
    adminAddr = "alexadhyatma@mail.ru";
    documentRoot = "/srv/http";
  # enableSSL = true;
  #  sslServerCert = ;
    enablePHP = true;
  };

  # DnsCrypt-Proxy
  services.dnscrypt-proxy = {
    enable = true;
    resolverName = "ipredator";
  };

  # Disable dhcpcd
  services.dhcpd.enable = false;
  networking.useDHCP = false;

  # SSD shit
  services.udev.extraRules = ''
   # set deadline scheduler for non-rotating disks
   ACTION=="add|change", KERNEL=="sd[a-z]", TEST!="queue/rotational", ATTR{queue/scheduler}="deadline"
   ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="deadline"

   # set cfq scheduler for rotating disks
   ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
  '';

  # PostgreSQL
  services.postgresql = {
    enable = false;
    package = pkgs.postgresql;
  };
}
