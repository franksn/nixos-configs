{config, pkgs, ...}:

{
 users.extraGroups =
 [
   { name = "plugdev"; gid = 10001; }
 ];

 services.udev.extraRules = ''
 ATTRS{idVendor}=="2717", ATTRS{idProduct}=="1268", \
   SUBSYSTEMS=="usb", ACTION=="add", MODE="0660", GROUP="plugdev"
 '';
}
