{config, pkgs, ...} :

{
  config =
  {
    environment = {
      systemPackages = with pkgs; [
	alsaLib
	alsaPlugins
	alsaUtils
	bar-xft
	bspwm
	chromium
	compton
	dmenu2
	feh
	ffmpegthumbnailer
	firefox
	gst_plugins_bad
	gst_plugins_base
	gst_plugins_good
	gst_python
	fzf
	jwm
	hsetroot
	llpp
	lxappearance
	mpc_cli
	mpd
	mpv
	mutt-with-sidebar
	ncmpcpp
	numix-gtk-theme
	python27Packages.discogs_client
	python27Packages.eyeD3
	python27Packages.pyacoustid
	python27Packages.pylast
	rxvt_unicode-with-plugins
	scrot
	sxhkd
	urxvt_perls
	wmutils-core
	xclip
	xfontsel
	xlibs.xinput
	xlibs.xmodmap
	xorg.mkfontdir
	xorg.mkfontscale
	zathura
	zathuraCollection.zathura_pdf_mupdf
      ];
    };
  };
}
