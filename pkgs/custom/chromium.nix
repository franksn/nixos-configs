{config, pkgs, lib, ...}:
with lib;
{
  config = {
    nixpkgs.config.chromium =
    {
      enablePepperFlash = true;
      enablePepperPDF = false;
    };
  };
}
