{config}:
{
  vimrcConfig.customRC = ''
  " General settings {{{
    syntax on
    filetype off
    filetype plugin indent on

    set title
    set mouse=a

    set shortmess=at      " shorten error messages

    set nrformats+=alpha  " in-/decrease letters with C-a/C-x

    set modeline          " enable modelines
    set modelines=5

    set number            " enable line numbers
    set ruler             " enable something
    set cursorline        " enable hiliting of cursor line

    set backspace=2       " backspace over EOL etc.

    set background=dark   " i prefer dark backgrounds

    set hidden            " buffer switching should be quick
    set confirm           " ask instead of just print errors
    set equalalways       " make splits equal size

    set lazyredraw        " don't redraw while executing macros

    set noshowmode        " don't display mode, it's already in the status line
    set pastetoggle=<F9>

    let mapleader=","
    let maplocalleader=","
    let g:nerdtree_tabs_open_on_console_startup=0
    let g:session_autosave = 'no'
  " }}}
  " General Keybinds {{{

    " Set MapLeader
    let mapleader = ","

    " Delete previous word with C-BS
    imap <C-BS> <C-W>

    " Toggle Buffer Selection and list Tag Lists
    map <F2> <Esc>:TSelectBuffer<CR>
    map <F4> <Esc>:TlistToggle<CR>

    " Set text wrapping toggles
    nmap <silent> <leader>w :set invwrap<CR>:set wrap?<CR> 

    " Set up retabbing on a source file
    nmap <silent> <leader>rr :1,$retab<CR>

    " cd to the directory containing the file in the buffer
    "nmap <silent> <leader>cd :lcd %:h<CR>

    " Make the directory that contains the file in the current buffer.
    " This is useful when you edit a file in a directory that doesn't
    " (yet) exist
    nmap <silent> <leader>md :!mkdir -p %:p:h<CR>

    " Increase @revision # by 1
    nmap <silent> <leader>incr /@updated

  " }}}
    " {{{ Window movement
    nmap <A-h> :winc h<CR>
    nmap <A-j> :winc j<CR>
    nmap <A-k> :winc k<CR>
    nmap <A-l> :winc l<CR>
  " }}}
  " GUI or no GUI, that's the question {{{
    if has('gui_running')
      set guicursor+=a:blinkon0       " Cursor doesn't blink - it's annoying
      set guioptions-=m               " No Menubar
      set guioptions-=T               " No Toolbar
      set guioptions-=l               " No Scrollbar left
      set guioptions-=L               " No Scrollbar left when split
      set guioptions-=r               " No Scrollbar right
      set guioptions-=r               " No Scrollbar right when split

      set laststatus=2                " always show statusline

      " set gfn=Pragmata\ 6.5
      " set gfn=uushi\ Medium\ 11
      set gfn=Hack\ 10

      set lines=40                    " Height
      set columns=85                  " Width
      colorscheme horse

    else
      colorscheme gruvbox-dark
    endif
  " }}}
  " Status line {{{
    set laststatus=2      " always show statusline
    "let g:lightline = {
    ""      \ 'colorscheme': 'erosion',
    ""      \ 'component': {
    ""      \   'readonly': '%{&readonly?"RO":""}',
    ""      \ },
    ""      \ 'separator': { 'left': '⮀', 'right': '⮂' },
    ""      \ 'subseparator': { 'left': '⮁', 'right': '⮃' }
    ""      \ }
    "let g:lightline = {
    ""      \ 'colorscheme': 'erosion',
    ""      \ 'component': {
    ""      \   'readonly': '%{&readonly?"⭤":""}',
    ""      \ }
    ""      \ }
  " Generic Statusline {{{
    function! SetStatus()
      setl statusline+=
        \%1*\ %f
        \%H%M%R%W%7*\ ┃
        \%2*\ %Y\ <<<\ %{&ff}%7*\ ┃
    endfunction

    function! SetRightStatus()
      setl statusline+=
        \%5*\ %{StatusFileencoding()}%7*\ ┃
        \%5*\ %{StatusBuffersize()}%7*\ ┃
        \%=%<%7*\ ┃
        \%5*\ %{StatusWrapON()}
        \%6*%{StatusWrapOFF()}\ %7*┃
        \%5*\ %{StatusInvisiblesON()}
        \%6*%{StatusInvisiblesOFF()}\ %7*┃
        \%5*\ %{StatusExpandtabON()}
        \%6*%{StatusExpandtabOFF()}\ %7*┃
        \%5*\ w%{StatusTabstop()}\ %7*┃
        \%3*\ %l,%c\ >>>\ %P
        \\
    endfunction " }}}
    " Update when leaving Buffer {{{
      function! SetStatusLeaveBuffer()
        setl statusline=""
        call SetStatus()
      endfunction
      au BufLeave * call SetStatusLeaveBuffer() " }}}
    " Update when switching mode {{{
      function! SetStatusInsertMode(mode)
        setl statusline=%4*
        if a:mode == 'i'
          setl statusline+=\ Insert\ ◥
        elseif a:mode == 'r'
          setl statusline+=\ Replace\ ◥
        elseif a:mode == 'v'
          setl statusline+=\ Visual\ ◥
        elseif a:mode == 'normal'
          setl statusline+=\ \ ◥
        endif
        call SetStatus()
        call SetRightStatus()
      endfunction"

    au VimEnter     * call SetStatusInsertMode('normal')
    au InsertEnter  * call SetStatusInsertMode(v:insertmode)
    au InsertLeave  * call SetStatusInsertMode('normal')
    au BufEnter     * call SetStatusInsertMode('normal') " }}}
    "fileencoding (three characters only) {{{
      function! StatusFileencoding()
        if &fileencoding == ""
          if &encoding != ""
            return &encoding
          else
            return " -- "
          endif
        else
          return &fileencoding
        endif
    endfunc " }}}
    " &expandtab {{{
      function! StatusExpandtabON()
        if &expandtab == 0
          return "tabs"
        else
          return ""
        endif
      endfunction "
      function! StatusExpandtabOFF()
        if &expandtab == 0
          return ""
        else
          return "tabs"
        endif
      endfunction " }}}
    " tabstop and softtabstop {{{
  function! StatusTabstop()

    " show by Vim option, not Cream global (modelines)
    let str = "" . &tabstop
    " show softtabstop or shiftwidth if not equal tabstop
    if   (&softtabstop && (&softtabstop != &tabstop))
    \ || (&shiftwidth  && (&shiftwidth  != &tabstop))
      if &softtabstop
        let str = str . ":sts" . &softtabstop
      endif
      if &shiftwidth != &tabstop
        let str = str . ":sw" . &shiftwidth
      endif
    endif
    return str

  endfunction " }}}
  " Buffer Size {{{
  function! StatusBuffersize()
    let bufsize = line2byte(line("$") + 1) - 1
    " prevent negative numbers (non-existant buffers)
    if bufsize < 0
      let bufsize = 0
    endif
    " add commas
    let remain = bufsize
    let bufsize = ""
    while strlen(remain) > 3
      let bufsize = "," . strpart(remain, strlen(remain) - 3) . bufsize
      let remain = strpart(remain, 0, strlen(remain) - 3)
    endwhile
    let bufsize = remain . bufsize
    " too bad we can't use "¿" (nr2char(1068)) :)
    let char = "b"
    return bufsize . char
  endfunction " }}}
  " Show Invisibles {{{
  function! StatusInvisiblesON()
    "if exists("g:LIST") && g:LIST == 1
    if &list
      if     &encoding == "latin1"
        return "¶"
      elseif &encoding == "utf-8"
        return "¶"
      else
        return "$"
      endif
    else
      return ""
    endif
  endfunction
  function! StatusInvisiblesOFF()
    "if exists("g:LIST") && g:LIST == 1
    if &list
      return ""
    else
      if     &encoding == "latin1"
        return "¶"
      elseif &encoding == "utf-8"
        return "¶"
      else
        return "$"
      endif
    endif
  endfunction " }}}
  " Wrap Enabled {{{
  function! StatusWrapON()
    if &wrap
      return "wrap"
    else
      return ""
    endif
  endfunction
  function! StatusWrapOFF()
    if &wrap
      return ""
    else
      return "wrap"
    endif
  endfunction
  " }}}
  " }}}
  " Tabstops {{{
  set tabstop=2
  set shiftwidth=2
  set softtabstop=2
  set autoindent
  set smartindent
  set expandtab
  " }}}
  " Invisibles {{{
  set listchars=tab:>\ ,eol:<
  set list
  nmap <silent> <F5> :set list!<CR>
  " }}}
  " Tabstops {{{
  set tabstop=2
  set shiftwidth=2
  set softtabstop=2
  set autoindent
  set smartindent
  set expandtab
  " }}}
  " Invisibles {{{
  set listchars=tab:>\ ,eol:<
  set list
  nmap <silent> <F5> :set list!<CR>
  " }}}
  " Folds {{{
  set foldmethod=marker
  set foldcolumn=1
  " au BufWinLeave * mkview
  " au BufWinEnter * silent loadview
  " }}}
  " Pairings {{{
  set showmatch
  " }}}
  " Margins {{{
  set scrolloff=5
  set sidescroll=5
  " }}}
  " Search {{{
  set incsearch
  set ignorecase

  " Toggle that stupid highlight search
  nmap <silent> ,n :set invhls<CR>:set hls?<CR> 
  " }}}
  " Backup files {{{
  set nobackup
  set nowb
  set noswapfile
  " }}}
  " Completion {{{
  set wildmenu
  set wildmode=longest,full,list

  set ofu=syntaxcomplete#Complete
  " }}}
  " NERDTree {{{
  map <F3> :NERDTreeToggle<CR>

  let NERDTreeChDirMode = 2
  let NERDTreeShowBookmarks = 1
  " }}}
  " Wrapping {{{
  set linebreak
  set showbreak=↳\ 
  " toggle wrapping
  nmap <silent> <F12> :let &wrap = !&wrap<CR>
  " }}}
  " 'Bubbling' {{{
  nmap <C-up> [e
  nmap <C-down> ]e
  vmap <C-up> [egv
  vmap <C-down> ]egv
  " }}}
  " Formatting with Par (gqip) {{{
  set formatprg=par\ -req
  nmap <F9> gqip
  " }}}
  " Pasting {{{
  "  set paste
  "  nnoremap p ]p
  "  nnoremap <c-p> p
  " }}}
  " Macros {{{
  " Execute macro "q" with space
  nmap <Space> @q
  " Map @ to + for more comfortable macros on DE kb layout
  nmap + @
  " }}}
  " JS LIB Syntax {{{
  let g:used_javascript_libs = 'angularjs,angularui,requirejs,jasmine' 
  " }}}
  " SyntaxComplete {{{
 if has("autocmd") && exists("+omnifunc")
   autocmd Filetype *
       \ if &omnifunc == "" |
       \ setlocal omnifunc=syntaxcomplete#Complete |
       \ endif
 endif
  " }}}
  " ctrlp plugin options {{{
  let g:ctrlp_map = '<c-p>'
  let g:ctrlp_cmd = 'CtrlP'
  let g:ctrlp_working_path_mode = 'ra'

  "exclude files and directories using wildignore and ctrlp
  set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
  set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows

  let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
  "  let g:ctrlp_custom_ignore = {
  "    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  "    \ 'file': '\v\.(exe|so|dll)$',
  "    \ 'link': 'some_bad_symbolic_links',
  "    \ }

  " Use a custom file listing command:
  let g:ctrlp_user_command = 'find %s -type f'
  " }}}
  " Neosnippet {{{
  " Plugin key-mappings
  imap <C-k>     <Plug>(neosnippet_expand_or_jump)
  smap <C-k>     <Plug>(neosnippet_expand_or_jump)
  xmap <C-k>     <Plug>(neosnippet_expand_target)

  " SuperTab like snippets behavior.
  imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
  \ "\<Plug>(neosnippet_expand_or_jump)"
  \: pumvisible() ? "\<C-n>" : "\<TAB>"
  smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
  \ "\<Plug>(neosnippet_expand_or_jump)"
  \: "\<TAB>"

  " For snippet_complete marker.
  if has('conceal')
  set conceallevel=2 concealcursor=i
  endif
  " }}}
  " {{{ Neocomplete
  " Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
  " Disable AutoComplPop.
  let g:acp_enableAtStartup = 0
  " Use neocomplete.
  let g:neocomplete#enable_at_startup = 1
  " Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplete#close_popup() . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

" For cursor moving in insert mode(Not recommended)
"inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
"inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
"inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
"inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"
" Or set this.
"let g:neocomplete#enable_cursor_hold_i = 1
" Or set this.
"let g:neocomplete#enable_insert_char_pre = 1

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
"}}}
  "Delimit Mate {{{
    "BreakLine: Return TRUE if in the middle of {} or () in INSERT mode
    "fun BreakLine()
    "  if (mode() == 'i')
    "    return ((getline(".")[col(".")-2] == '{' && getline(".")[col(".")-1] == '}') ||
    "       \(getline(".")[col(".")-2] == '(' && getline(".")[col(".")-1] == ')'))
    "  else
    "    return 0
    "  endif
    "endfun

    " Remap <Enter> to split the line and insert a new line in between if
    " BreakLine return True
    "inoremap <expr> <CR> BreakLine() ? "<CR><ESC>O" : "<CR>"
  "}}}
  " rainbow parentheses {{{
    autocmd VimEnter * RainbowParentheses
    let g:rainbow#max_level = 16
    let g:rainbow#pairs = [['(', ')'], ['[', ']']]
    let g:rbpt_colorpairs = [
      \ ['brown',       'RoyalBlue3'],
      \ ['Darkblue',    'SeaGreen3'],
      \ ['darkgray',    'DarkOrchid3'],
      \ ['darkgreen',   'firebrick3'],
      \ ['darkcyan',    'RoyalBlue3'],
      \ ['darkred',     'SeaGreen3'],
      \ ['darkmagenta', 'DarkOrchid3'],
      \ ['brown',       'firebrick3'],
      \ ['gray',        'RoyalBlue3'],
      \ ['black',       'SeaGreen3'],
      \ ['darkmagenta', 'DarkOrchid3'],
      \ ['Darkblue',    'firebrick3'],
      \ ['darkgreen',   'RoyalBlue3'],
      \ ['darkcyan',    'SeaGreen3'],
      \ ['darkred',     'DarkOrchid3'],
      \ ['red',         'firebrick3'],
      \ ]
  " }}}
  " {{{ ---- RUST ---------
    " set hidden
    "let g:racer_cmd = "/usr/bin/racer"
    "let $RUST_SRC_PATH= "/usr/src/rust/src/"
  " }}}

  '';
}
