{config, pkgs, ...} :

{
  config =
  {
    environment = {
      systemPackages = with pkgs; [
	aria
	autoconf
	automake
	aspell
	aspellDicts.en
	beets
	cv
	dnscrypt-proxy
	figlet
	file
	gcc
	gnupg
	gptfdisk
	hddtemp
	hdparm
	htop
	imagemagick
	imlib2
	irssi
	isync
	jmtpfs
	keychain
	libcaca
	lm_sensors
	lsof
	mdp
	mksh
	msmtp
	newsbeuter
	ntfs3g
	parted
	pass
	pinentry
	pmount
	pmutils
	psmisc
	ranger
	smartmontools
	tmux
	tokyocabinet
	unzip
	w3m
	weechat
	wget
	which
	zip
	zsh
      ];
    };
  };
}
