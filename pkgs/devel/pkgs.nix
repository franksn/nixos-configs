{config, pkgs, ...} :

{
  config =
  {
    environment = {
      systemPackages = with pkgs; [
	apacheHttpd
	bc
	cargo
	cmake
	colordiff
	cryptsetup
	elixir
	emacs
	erlang
	ghostscript
	gitAndTools.gitFull
	go
	gocode
	(let myTexLive =
	  pkgs.texLiveAggregationFun {
	    paths =
	      [ pkgs.texLive
#		pkgs.texLiveExtra
	      ];
	  };
	in myTexLive)
	jdk8
	leiningen
	lua
	#mercurial
	nix-repl
	nodejs
	npm2nix
	php
	postgresql
	python
	python27Packages.virtualenvwrapper
	python34Packages.virtualenvwrapper
	ruby
	rustc
      ];
    };
  };
}
