# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, ... }:

{

  boot = {
#    kernelPackages = pkgs.linuxPackages_4_2;
    initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "usb_storage" "usbhid" ];
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
  };

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/9101900b-d69d-4071-9073-6eb0fbffabd0";
      fsType = "ext4";
      options = "defaults,noatime,discard";
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/38d9ccec-4f24-4181-9c56-600e8bdb66a2";
      fsType = "ext4";
      options = "defaults,noatime";
    };

  fileSystems."/nix" =
    { device = "/dev/disk/by-uuid/1be272a0-ca82-4cb2-bf9a-5c4d17c37f2b";
      fsType = "ext4";
      options = "defaults,noatime,discard";
    };

  fileSystems."/var" =
    { device = "/dev/disk/by-uuid/3d5119f3-5555-4344-9bf6-95706435a46a";
      fsType = "ext4";
      options = "defaults,noatime,discard";
    };

  fileSystems."/home/datas" =
    { device = "/dev/disk/by-uuid/7360-0A10";
      fsType = "vfat";
      options = "uid=1000,gid=100,fmask=133,umask=022,utf8,uni_xlate,relatime";
    };

  fileSystems."/home/media" =
    { device = "/dev/disk/by-uuid/efeb24d7-94e5-49ca-b22e-aa0d9ac7327d";
      fsType = "xfs";
      options = "defaults,relatime";
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/f65d563c-33ca-46db-aa1d-201d930bb770";
      fsType = "ext2";
    };

  fileSystems."/tmp" =
    { device = "tmpfs";
      fsType = "tmpfs";
      options = "nosuid,nodev,relatime,size=12G";
    };

  swapDevices = [ ];

  nix.maxJobs = 8;
  nix.buildCores = 8;
}
