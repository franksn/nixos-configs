# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }: 
{
  imports =
    [ 
      ./hardware-configuration.nix
      ./configs/nixopts.nix
      ./machines/hw-config.nix
      ./pkgs/base/pkgs.nix
      ./pkgs/custom/chromium.nix
      ./pkgs/devel/pkgs.nix
      ./pkgs/x11/pkgs.nix
      ./private/net.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sdb";

  networking.hostName = "Bonecaster"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  i18n = {
     consoleFont = "Lat2-Terminus16";
     consoleKeyMap = "us";
     defaultLocale = "en_US.UTF-8";
   };

  # Set your time zone.
  time.timeZone = "Asia/Jakarta";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.extraUsers.guest = {
  #   isNormalUser = true;
  #   uid = 1000;
  # };

  fonts = {
      enableCoreFonts = false;
      enableFontDir = true;
      enableGhostscriptFonts = false;
      fonts = [
         #pkgs.cantarell_fonts
         #pkgs.dosemu_fonts
         #pkgs.freefont_ttf
         pkgs.liberation_ttf
         #pkgs.ucsFonts
         #pkgs.unifont
         #pkgs.vistafonts
         #pkgs.xlibs.fontadobe100dpi
         #pkgs.xlibs.fontadobe75dpi
         #pkgs.xlibs.fontadobeutopia100dpi
         #pkgs.xlibs.fontadobeutopia75dpi
         #pkgs.xlibs.fontadobeutopiatype1
         #pkgs.xlibs.fontarabicmisc
         #pkgs.xlibs.fontbh100dpi
         #pkgs.xlibs.fontbh75dpi
         #pkgs.xlibs.fontbhlucidatypewriter100dpi
         #pkgs.xlibs.fontbhlucidatypewriter75dpi
         #pkgs.xlibs.fontbhttf
         #pkgs.xlibs.fontbhtype1
         #pkgs.xlibs.fontbitstream100dpi
         #pkgs.xlibs.fontbitstream75dpi
         #pkgs.xlibs.fontbitstreamtype1
         #pkgs.xlibs.fontcronyxcyrillic
         #pkgs.xlibs.fontxfree86type1
      ];
    };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "16.03";

}
